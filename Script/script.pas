﻿ var
    kundenAuswahl : string;
    dateiName : string;
    kundeFuerDateiName : string;
    eingangsDateiname : string;

    // Einstellungen:
    pfadEingang : string;
    pfadArchiv : string;
    pfadSync : string;
    pfadAusgang : string;

    idKundeUnbekannt : integer;
    Web : TWebBrowser;

//============================================================= Einstellungen

procedure Hauptfenster_Image1_OnClick (Sender: string);
begin
    OpenURL('http://lusolution.roehrach.de/forum/');
end;

procedure Hauptfenster_Button6_OnAfterClick (Sender: string);
begin                                   
    Hauptfenster.BtnSucheKunde.Click;
end;

procedure Hauptfenster_Button5_OnAfterClick (Sender: string);
begin
    Hauptfenster.BtnSucheKunde.Click;
end;     

procedure Einstellungen_OnShow (Sender: string; Action: string);
begin
    if Length(Einstellungen.EdtEingang.Text) = 0 then
    begin
        Einstellungen.EdtEingang.Text := ExtractFileDir(Application.ExeName) + '\Eingang';
    end;

    if Length(Einstellungen.EdtAusgang.Text) = 0 then
    begin
        Einstellungen.EdtAusgang.Text := ExtractFileDir(Application.ExeName) + '\Ausgang';
    end;
end;

procedure einstellungenUebernehmen();
var
    Results: TDataSet;
    hdSerial : string;
begin

    hdSerial := GetHardDiskSerial('C:');

    SQLQuery('SELECT PfadEingang, PfadArchiv, PfadSync, PfadAusgang FROM Einstellungen WHERE PcId="' + hdSerial + '" ORDER BY ID DESC', Results);
    if not Results.Eof then
    begin
         pfadEingang := Results.FieldByName('PfadEingang').asString;
         pfadArchiv := Results.FieldByName('PfadArchiv').asString;
         pfadSync := Results.FieldByName('PfadSync').asString;
         pfadAusgang := Results.FieldByName('PfadAusgang').asString;
    end;

    Einstellungen.EdtEingang.Text := pfadEingang;
    Einstellungen.EdtArchiv.Text := pfadArchiv;
    Einstellungen.EdtSync.Text := pfadSync;                
    Einstellungen.EdtAusgang.Text := pfadAusgang;
    Einstellungen.EdtPcId.Text := hdSerial;
end;

procedure Einstellungen_BtnScannerEinst_OnClick (Sender: string; var Cancel: boolean);
begin
   OpenFile('/SOURCE','.\CmdTwain\CmdTwain.exe');
end;

procedure Einstellungen_BtnEinstSpeichern_OnAfterClick (Sender: string);
begin
     einstellungenUebernehmen();
end;



//============================================================= Hauptfenster

procedure Hauptfenster_OnShow (Sender: string; Action: string);
begin
     Hauptfenster.TabsHaupt.ActivePageIndex := 0;

     Begruessung.ShowModal;

     einstellungenUebernehmen();

     if not DirectoryExists(pfadEingang) or not DirectoryExists(pfadArchiv) then
     begin
         Einstellungen.ShowModal;
     end;

     einstellungenUebernehmen();

     Hauptfenster.BtnSuchDok.Click;
     Hauptfenster.BtnSucheKunde.Click;

     Hauptfenster.EdtKId.Text := Hauptfenster.TgKunden.Cells[3,0];

     aktualisiereEingang();
end;

procedure Hauptfenster_EdtSuchKunde_OnChange (Sender: string);
begin
     Hauptfenster.BtnSucheKunde.Click;
end;

procedure Hauptfenster_EdtSuchDok_OnChange (Sender: string);
begin
     Hauptfenster.BtnSuchDok.Click;
end;

procedure Hauptfenster_TgDokumente_OnCellDoubleClick (Sender: string; ACol, ARow: Integer);
var
    tmp : string;
begin
    tmp := Hauptfenster.TgDokumente.Cells[5, ARow];

    if not FileExists(tmp) then
    begin
        ShowMessage('Datei existiert nicht !!!');
    end;
    OpenFile(tmp);
end;

procedure aktualisiereEingang();
var
   all : string;
   fileNames : array of string;
   i : integer;
begin

       if not DirectoryExists(pfadEingang) then Exit;

       all := GetFilesList(pfadEingang + '\');
       all := ReplaceStr(all, #13#10, ';');

       fileNames := SplitString(all, ';');

       SQLExecute('CREATE TEMPORARY TABLE IF NOT EXISTS Eingang (id INTEGER PRIMARY KEY AUTO_INCREMENT, Dateiname TEXT)');

       SQLExecute('DELETE FROM Eingang');
       
       for i:= 0 to Length(fileNames) - 2 do
       begin
           SQLExecute('INSERT INTO Eingang (Dateiname) VALUES ("' + ExtractFileName(fileNames[i]) + '")');
       end;

       Hauptfenster.TgDokEingang.dbSQL := 'SELECT Dateiname, id FROM Eingang';
       Hauptfenster.TgDokEingang.dbSQLExecute;
end;

procedure Hauptfenster_BtnAktualisieren_OnClick (Sender: string; var Cancel: boolean);
begin
      aktualisiereEingang();
end;

procedure Hauptfenster_TabsHaupt_OnChange (Sender: string);
begin
     if Hauptfenster.TabsHaupt.ActivePageIndex = 2 then
     begin                        
         aktualisiereEingang();
     end;
end;

procedure Hauptfenster_BtnScan_OnClick (Sender: string; var Cancel: boolean);
begin
     OpenFile(' -c "A4 DPI 100 RGB DPX 0" pdf "' + pfadEingang + '\' + FormatDateTime('YYYY-MM-DD_hh-mm-ss', Now) + '_Scan.pdf"','.\CmdTwain\CmdTwain.exe');
end;

procedure Hauptfenster_TgDokEingang_OnCellClick (Sender: string; ACol, ARow: Integer);
var
    tokens, name, vorname : array of string;
    kundenname : string;
    anzahl : integer;
begin
     eingangsDateiname := Hauptfenster.TgDokEingang.Cells[0,ARow];

     kundenname := '';

     tokens := SplitString(ExtractFileName(eingangsDateiname), '_');

     if Length(tokens) > 1 then
     begin

         kundenname := tokens[0] + ' ' + tokens[1];
         anzahl := SQLExecute('SELECT COUNT(id) FROM Kunden WHERE (CONCAT(Name," ",Vorname) LIKE "%' + kundenname + '%" OR CONCAT(Vorname," ",Name) LIKE "%' + kundenname +'%")');

         if anzahl < 1 then kundenname := '';
         Aufnehmen.EdtSuche.Text := kundenname;
         Exit;
     end;

     tokens := SplitString(ExtractFileName(eingangsDateiname), ',');

     if Length(tokens) > 1 then
     begin
         name := SplitString(Trim(tokens[0]), ' ');
         vorname := SplitString(Trim(tokens[1]), ' ');
    
         kundenname := Trim(name[0]) + ' ' + Trim(vorname[0]);

         anzahl := SQLExecute('SELECT COUNT(id) FROM Kunden WHERE (CONCAT(Name," ",Vorname) LIKE "%' + kundenname + '%" OR CONCAT(Vorname," ",Name) LIKE "%' + kundenname +'%")');
                                                                                                
         if anzahl < 1 then kundenname := '';
     end;
                        
     Aufnehmen.EdtSuche.Text := kundenname;
end;

procedure Hauptfenster_TgDokEingang_OnCellDoubleClick (Sender: string; ACol, ARow: Integer);
var
     fileName : string;
begin
     fileName := Hauptfenster.TgDokEingang.Cells[ACol,ARow];
     OpenFile(pfadEingang + '\' + fileName);
end;


procedure Hauptfenster_BtnEingangLoeschen_OnClick (Sender: string; var Cancel: boolean);
var
    datei : string;
    ok : boolean;
begin
     datei := Hauptfenster.TgDokEingang.Cells[0,Hauptfenster.TgDokEingang.SelectedRow];

     ok := DeleteFile(pfadEingang + '\' + datei);

     if not ok then
     begin
         ShowMessage('Datei wird verwendet - Löschen nicht möglich !!!');
     end;

     aktualisiereEingang();
end;


procedure Hauptfenster_BtnSync_OnClick (Sender: string; var Cancel: boolean);
begin
    DeleteFile('Toucan\Data\Jobs.ini');
    WriteLnToFile('Toucan\Data\Jobs.ini', '[General]');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Version=301');
    WriteLnToFile('Toucan\Data\Jobs.ini', '[Sync]');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Source=' + ReplaceStr(pfadArchiv,'\','\\'));
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Dest=' + ReplaceStr(pfadSync,'\','\\'));
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Function=Equalise');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'CheckSize=1');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'CheckTime=1');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'CheckShort=1');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'CheckFull=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'TimeStamps=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Attributes=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'IgnoreReadOnly=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Recycle=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'PreviewChanges=0');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Rules=');
    WriteLnToFile('Toucan\Data\Jobs.ini', 'Type=Sync');

    OpenFile('','Toucan\startToucan.bat');
end;

procedure Hauptfenster_BtnAusgang_OnClick (Sender: string; var Cancel: boolean);
var
    dateiNameAlt : string;
    dateiNameNeu : string;
begin
    dateiNameAlt := Hauptfenster.TgDokumente.Cells[2, Hauptfenster.TgDokumente.SelectedRow];
    //dateiNameAlt := pfadArchiv + '\' + dateiNameAlt;

    dateiNameNeu := pfadAusgang + '\' + ExtractFileName(dateiNameAlt);

    CopyFile(dateiNameAlt,dateiNameNeu);
end;

procedure Hauptfenster_BtnKDokAusgang_OnClick (Sender: string; var Cancel: boolean);
var
    dateiNameAlt : string;
    dateiNameNeu : string;
begin
    dateiNameAlt := Hauptfenster.TgKDok.Cells[5, Hauptfenster.TgKDok.SelectedRow];

    dateiNameNeu := pfadAusgang + '\' + ExtractFileName(dateiNameAlt);

    CopyFile(dateiNameAlt,dateiNameNeu);
end;

procedure Hauptfenster_TgKunden_OnCellClick (Sender: string; ACol, ARow: Integer);
begin
    Hauptfenster.EdtKId.Text := Hauptfenster.TgKunden.Cells[3,ARow];
end;

procedure Hauptfenster_TgKunden_OnCellDoubleClick (Sender: string; ACol, ARow: Integer);
begin
    Hauptfenster.EdtKId.Text := Hauptfenster.TgKunden.Cells[3,ARow];
    Hauptfenster.TabsHaupt.ActivePageIndex := 1;
end;


procedure Hauptfenster_EdtKId_OnChange (Sender: string);
var
    name, vorname : string;
begin

    name := SQLExecute('SELECT Name FROM Kunden WHERE id = ' + Hauptfenster.EdtKId.Text );
    vorname := SQLExecute('SELECT Vorname FROM Kunden WHERE id = ' + Hauptfenster.EdtKId.Text );

    Hauptfenster.LblKundenName.Caption := name + ' ' + vorname;
                     
    Hauptfenster.BtnKDokSuche.Click;
end;

procedure Hauptfenster_TgKDok_OnCellDoubleClick (Sender: string; ACol, ARow: Integer);
var
    tmp : string;
begin
    tmp := Hauptfenster.TgKDok.Cells[5, ARow];

    if not FileExists(tmp) then
    begin
        ShowMessage('Datei existiert nicht !!!');
    end;
    OpenFile(tmp);
end;

procedure Hauptfenster_EdtKDokSuche_OnChange (Sender: string);
begin
    Hauptfenster.BtnKDokSuche.Click;
end;

//============================================================= Aufnehmen

procedure createHtml(filename : string);
var
    html : string;
    path : string;
begin
    html := '<html><body><iframe src="';
    html := html + filename;
    html := html + '" width="100%" height="100%"></iframe></body></html>';

    path := ExtractFileDir(Application.ExeName) + '\Script\preview.html';

    DeleteFile(path);

    WriteLnToFile(path, html);
end;


procedure Aufnehmen_OnShow (Sender: string; Action: string);
var
    ext : string;
begin
    Aufnehmen.EdtBezeichnung.Clear;
    Aufnehmen.CmbBezeichnung.dbSQLExecute('SELECT Bezeichnung FROM Bezeichnungen');
    Aufnehmen.BtnSuche.Click;
    Aufnehmen.EdtQuelle.Text := eingangsDateiname;

    Web.Width := Aufnehmen.Panel1.Width;
    Web.Height := Aufnehmen.Panel1.Height;

    createHtml( pfadEingang + '\' + eingangsDateiname );

    ext := ExtractFileExt(eingangsDateiname);

    Web.Navigate(ExtractFileDir(Application.ExeName) + '\Images\lu-soft.jpg');

    if PosEx(Lowercase(ext),'.pdf.jpg.jpeg.txt.html.htm.png.gif') <> 0 then
    begin
        Web.Navigate( ExtractFileDir(Application.ExeName) + '\Script\preview.html' );
    end;

    Aufnehmen.BtnAufnehmen.Enabled := False;
    Aufnehmen.EdtDateiNameNeu.Clear;
    Aufnehmen.EdtBemerkung.Clear;

end;

procedure Aufnehmen_OnClose (Sender: string; Action: string);
begin
    Aufnehmen.EdtKunde.Clear;
    Web.Navigate(ExtractFileDir(Application.ExeName) + '\Images\lu-soft.jpg');
end;

procedure Aufnehmen_BtnAufnehmen_OnAfterClick (Sender: string);
var
    result : integer;
    dateiname : string;
    ok : boolean;
begin

    Web.Navigate(ExtractFileDir(Application.ExeName) + '\Images\lu-soft.jpg');
    
    dateiname :=  pfadEingang + '\' + eingangsDateiname;
    
    CopyFile(dateiname, Aufnehmen.EdtDateiNameNeu.Text);
    
    result := MessageBox('Soll die Datei jetzt aus dem Eingang entfernt werden ?','Datei entfernen ?', MB_ICONQUESTION+MB_YESNO);
    
    if result = IDYES then
    begin
        ok := DeleteFile(dateiname);
        
        if not ok then
        begin
         ShowMessage('Datei wird verwendet - Löschen nicht möglich !!!');
        end;

        aktualisiereEingang();
    end;
end;

procedure Aufnehmen_DtpDatum_OnChange (Sender: string);
begin
    dateinameAktualisieren();
end;

procedure Aufnehmen_EdtBezeichnung_OnChange (Sender: string);
begin
     dateinameAktualisieren();
end;

procedure dateinameAktualisieren();
var
    datumFuerDateiName : string;
    bezeichnungFuerDateiName : string;
begin
    datumFuerDateiName := FormatDateTime('YYYY-MM-DD', Aufnehmen.DtpDatum.DateTime);
    bezeichnungFuerDateiName := Aufnehmen.EdtBezeichnung.Text;
    bezeichnungFuerDateiName := ReplaceStr(bezeichnungFuerDateiName,' ','_');
    dateiName := pfadArchiv + '\' + kundeFuerDateiName + '_' + datumFuerDateiName + '_' + bezeichnungFuerDateiName + ExtractFileExt(eingangsDateiname);
    Aufnehmen.EdtDateiNameNeu.Text := dateiName;
end;

procedure Aufnehmen_TgKunden_OnCellClick (Sender: string; ACol, ARow: Integer);
begin
    kundenAuswahl := Aufnehmen.TgKunden.Cells[0,ARow];
    Aufnehmen.EdtKunde.Text := kundenAuswahl;
    kundeFuerDateiName := Aufnehmen.TgKunden.Cells[1,ARow] + '_' + Aufnehmen.TgKunden.Cells[2,ARow];

    Aufnehmen.CbKunde.dbItemID := StrToInt(kundenAuswahl);

    dateinameAktualisieren();

    Aufnehmen.BtnAufnehmen.Enabled := True;
end;


procedure Aufnehmen_EdtSuche_OnChange (Sender: string);
begin
     Aufnehmen.BtnSuche.Click;
end;

procedure Aufnehmen_BtnPlus_OnClick (Sender: string; var Cancel: boolean);
begin
    SQLExecute('INSERT INTO Bezeichnungen (Bezeichnung) VALUES ("' + Aufnehmen.EdtBezeichnung.Text + '")');
    Aufnehmen.CmbBezeichnung.dbSQLExecute('SELECT Bezeichnung FROM Bezeichnungen');
end;

procedure Aufnehmen_BtnMinus_OnClick (Sender: string; var Cancel: boolean);
begin
    SQLExecute('DELETE FROM Bezeichnungen WHERE Bezeichnung="' + Aufnehmen.EdtBezeichnung.Text + '"');
    Aufnehmen.CmbBezeichnung.dbSQLExecute('SELECT Bezeichnung FROM Bezeichnungen');
end;

procedure Aufnehmen_CmbBezeichnung_OnChange (Sender: string);
begin
    Aufnehmen.EdtBezeichnung.Text := Aufnehmen.CmbBezeichnung.Text;
end;


begin
    Web := TWebBrowser.Create(Aufnehmen.Panel1);
    TWinControl(Web).Parent := Aufnehmen.Panel1;
    Web.Navigate(ExtractFileDir(Application.ExeName) + '\Images\lu-soft.jpg');
end.

